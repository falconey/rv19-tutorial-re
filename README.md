# rv19-tutorial-re
This repository contains the material related to the RV 2019 tutorial


>  On the Runtime Enforcement of Timed Properties
<br>Yliès Falcone and Srinivas Pinisetty

The repository currently contains:
- the slides presented during the tutorial;
- the related papers;
- a virtual machine with TiPeX and dependencies installed;
- a video demonstrating TiPeX.
